package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.model.LineItem;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
public class BootstrapAppConfig {

    private final OrderRepository orderRepository;
    private final Faker faker = new Faker();

    @EventListener(ApplicationReadyEvent.class)
    public void initializeDB(ApplicationReadyEvent applicationReadyEvent){
        IntStream.range(0, 101).forEach(index -> {
            //Order order = new Order(12, 23, "Ramesh", "Nanda", true, true, false);
            Order order = Order.builder()
                                    .customerName(faker.name().fullName())
                                    .customerEmail(faker.internet().emailAddress())
                                    .price(faker.number().randomDouble(2, 2000, 4000))
                                .build();

            IntStream.range(0, 2)
                    .forEach(value -> {
                        LineItem item = LineItem.builder()
                                .name(faker.commerce().productName())
                                .qty(faker.number().numberBetween(2,5))
                                .price(faker.number().randomDouble(2, 200, 400))
                                .build();
                        order.addLineItem(item);
                    });
            this.orderRepository.save(order);
        });
    }

}

package com.classpath.ordermicroservice.event;

public enum OrderStatus {
    ORDER_ACCEPTED,
    ORDER_CANCELLED,
    ORDER_REJECETED,
    ORDER_FULFILLED
}

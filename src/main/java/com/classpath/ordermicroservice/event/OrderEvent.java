package com.classpath.ordermicroservice.event;

import com.classpath.ordermicroservice.model.Order;
import lombok.*;

import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public final class OrderEvent {
    private Order order;
    private LocalDateTime eventTime;
    private OrderStatus orderStatus;
}

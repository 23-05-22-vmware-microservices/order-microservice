package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.event.OrderEvent;
import com.classpath.ordermicroservice.event.OrderStatus;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {
    private final OrderRepository orderRepository;
    private final WebClient webClient;

    @Transactional
    @CircuitBreaker(name = "inventorymicroservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order){
        log.info("saving the order , {}", order.toString());
        Order savedOrder = this.orderRepository.save(order);
        //update the inventory using the rest call
        //generate an event and store in Kafka
        OrderEvent orderEvent = new OrderEvent(savedOrder, LocalDateTime.now(), OrderStatus.ORDER_ACCEPTED);
        return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while fetching the details :: {}", exception.getMessage());
        return Order.builder().build();
    }

    public Set<Order> fetchAllOrders(){
        log.info("fetching all the orders");
        return new HashSet(this.orderRepository.findAll());
    }

    public Order fetchOrderById(long orderId){
        log.info("fetching the order by id , {}", orderId);
        return this.orderRepository.findById(orderId)
                                    .orElseThrow(() -> new IllegalArgumentException("invalid order id"));
    }

    public void deleteOrderById(long orderId){
        log.info("deleting the order by id , {}", orderId);
        this.orderRepository.deleteById(orderId);
    }
}

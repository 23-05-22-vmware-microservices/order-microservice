package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    public Set<Order> fetchOrders(){
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable("id") long orderId){
        return this.orderService.fetchOrderById(orderId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order saveOrder(@RequestBody Order order) {
        return this.orderService.saveOrder(order);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable("id") long orderId){
        this.orderService.deleteOrderById(orderId);
    }
}
